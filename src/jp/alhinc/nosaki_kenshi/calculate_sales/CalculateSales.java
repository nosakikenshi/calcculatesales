package jp.alhinc.nosaki_kenshi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main (String[] args) {

		Map<String, String> branchMap = new HashMap<>();
		Map<String, Long> salesMap = new HashMap<>();
		List<Integer> salesList = new ArrayList<>();

		BufferedReader br = null;
		try {
			try {
				File branchLst = new File(args[0], "branch.lst");
				if(!branchLst.exists()) {
					System.out.println("支店定義ファイルが存在しません");
					return;
				}
				br = new BufferedReader(new FileReader(branchLst));
				String line;
				while((line = br.readLine()) != null) {
					if(!line.matches("[0-9]{3},(?!.*,).*")) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					String[] lines = line.split(",");
					branchMap.put(lines[0], lines[1]);
					salesMap.put(lines[0], 0L);
				}
			} finally {
				if(br != null) {
					br.close();
				}
			}

			File directory = new File(args[0]);
			String[] fileNames = directory.list();

			for(int i = 0; i < fileNames.length; i++) {
				int point = fileNames[i].lastIndexOf(".");
				String extension = fileNames[i].substring(point + 1);
				String woext = fileNames[i].substring(0, point);
				if(extension.equals("rcd") && woext.matches("[0-9]{8}")){
					salesList.add(Integer.parseInt(woext));

					try {
						File rcdFile = new File(args[0], fileNames[i]);
						br = new BufferedReader(new FileReader(rcdFile));

						String x = br.readLine();
						long y = Long.parseLong(br.readLine());
						if(br.readLine() != null) {
							System.out.println(fileNames[i] + "のフォーマットが不正です");
							return;
                        }

						if(!branchMap.containsKey(x)) {
							System.out.println(fileNames[i] + "の支店コードが不正です");
							return;
                        }

                        salesMap.put(x, salesMap.get(x) + y);
                        if(salesMap.get(x).toString().length() > 10) {
                            System.out.println("合計金額が10桁を超えました");
                            return;
                        }
					} finally{
						if(br != null) {
							br.close();
						}
					}
				}
			}
			Collections.sort(salesList);
			if(salesList.size() != salesList.get(salesList.size() - 1) - salesList.get(0) + 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}

//			集計結果出力
			File rcdFile = new File(args[0], "branch.out");
			BufferedWriter bw = new BufferedWriter(new FileWriter(rcdFile));

			for(String key : salesMap.keySet()) {
                bw.write(key + "," + branchMap.get(key) +  "," + salesMap.get(key));
                bw.newLine();
			}
			bw.close();

		} catch(Exception e) {
			System.out.println("予期せぬエラーが発生しました。");
		}
	}
}
